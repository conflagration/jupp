﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Controls;
using System.Windows.Data;

namespace JUPP
{
    public class GlobalVars
    {
        public const string DISCHARGED_STR = @"Wypisany";
        public const string MZN_DATA_FILE = @"data\duties.dzn";
        public static readonly string MZN_COMPILER_FILE = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles),
            @"MiniZinc IDE (bundled)\minizinc.exe");
        public static readonly string MZN_DATA_BACKUP_FILE = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData),
            @"JUPP\duties.dzn");

        public const string DATE_FORMAT = "dd.MM.yyyy";
    }

    public class DisplayedValue : System.Attribute
    {
        public DisplayedValue(string value, int order)
        {
            Value = value;
            Order = order;
        }

        public static IEnumerable<PropertyInfo> GetOrderedProperties(Type t)
        {
            return t.GetProperties().OrderBy(p => p.GetCustomAttribute<DisplayedValue>().Order);
        }
        public string Value;
        public int Order;
    }

    public class Pair : INotifyPropertyChanged
    {
        private Patient a;
        private Patient b;


        public Pair(Patient p1, Patient p2)
        {
            a = p1;
            b = p2;
        }
        [DisplayedValue("A", 1)]
        public Patient A
        {
            get => a;
            set
            {
                if (a != value)
                {
                    a = value;
                    NotifyPropertyChanged(nameof(A));
                }
            }
        }
        [DisplayedValue("B", 2)]
        public Patient B
        {
            get => b;
            set
            {
                if (b != value)
                {
                    b = value;
                    NotifyPropertyChanged(nameof(B));
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void NotifyPropertyChanged(string propName)
        {
            this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propName));
        }

        override public string ToString()
        {
            return "(" + A + "," + B + ")";
        }
    }

    public interface IPatientConst
    {
        string Name { get; }
        bool AvailableWeek1 { get; }
        bool AvailableWeek2 { get; }
        bool AvailableWeek3 { get; }
        bool AvailableWeek4 { get; }
        uint AvailableSumFrom { get; }
        uint AvailableSumTo { get; }
        bool IsSelected { get; }
    }

    public class Patient : IPatientConst, INotifyPropertyChanged, IEditableObject
    {
        private string name;
        private bool availableWeek1;
        private bool availableWeek2;
        private bool availableWeek3;
        private bool availableWeek4;
        private uint availableSumFrom;
        private uint availableSumTo;
        private bool isSelected;

        public Patient()
        {
            name = "Imie_Nazwisko";
            availableWeek1 = true;
            availableWeek2 = true;
            availableWeek3 = true;
            availableWeek4 = true;
            availableSumFrom = 0;
            availableSumTo = 4;
        }

        public Patient(string n)
        {
            name = n;
        }

        public string Name
        {
            get => name;
            set
            {
                var val = Encoding.ASCII.GetString(Encoding.GetEncoding("Cyrillic").
                    GetBytes(Regex.Replace(value.Trim(), @"\s", "_")));
                if (name != val)
                {
                    name = val;
                    if (!IsInEdit()) NotifyPropertyChanged(nameof(Name));
                }
            }
        }

        public bool AvailableWeek1
        {
            get => availableWeek1;
            set
            {
                if (availableWeek1 != value)
                {
                    availableWeek1 = value;
                    if (!IsInEdit()) NotifyPropertyChanged(nameof(AvailableWeek1));
                }
            }
        }

        public bool AvailableWeek2
        {
            get => availableWeek2;
            set
            {
                if (availableWeek2 != value)
                {
                    availableWeek2 = value;
                    if (!IsInEdit()) NotifyPropertyChanged(nameof(AvailableWeek2));
                }
            }
        }

        public bool AvailableWeek3
        {
            get => availableWeek3;
            set
            {
                if (availableWeek3 != value)
                {
                    availableWeek3 = value;
                    if (!IsInEdit()) NotifyPropertyChanged(nameof(AvailableWeek3));
                }
            }
        }

        public bool AvailableWeek4
        {
            get => availableWeek4;
            set
            {
                if (availableWeek4 != value)
                {
                    availableWeek4 = value;
                    if (!IsInEdit()) NotifyPropertyChanged(nameof(AvailableWeek4));
                }
            }
        }

        public uint AvailableSumFrom
        {
            get => availableSumFrom;
            set
            {
                if (availableSumFrom != value)
                {
                    availableSumFrom = value;
                    if (!IsInEdit()) NotifyPropertyChanged(nameof(AvailableSumFrom));
                }
            }
        }

        public uint AvailableSumTo
        {
            get => availableSumTo;
            set
            {
                if (availableSumTo != value)
                {
                    availableSumTo = value;
                    if (!IsInEdit()) NotifyPropertyChanged(nameof(AvailableSumTo));
                }
            }
        }

        public bool IsSelected
        {
            get => isSelected;
            set
            {
                if (isSelected != value)
                {
                    isSelected = value;
                    NotifyPropertyChanged(nameof(IsSelected));
                }
            }
        }

        public bool IsInEdit()
        {
            return backupCopy != null;
        }

        public IPatientConst Valid()
        {
            return IsInEdit() ? backupCopy : this;
        }

        private IPatientConst backupCopy;


        public void BeginEdit()
        {
            if (IsInEdit()) return;
            backupCopy = this.MemberwiseClone() as Patient;
        }

        public void CancelEdit()
        {
            if (!IsInEdit()) return;
            foreach (var p in typeof(Patient).GetProperties())
            {
                p.SetValue(this, p.GetValue(backupCopy));
            }
            backupCopy = null;
        }

        public void EndEdit()
        {
            if (!IsInEdit()) return;
            foreach (var p in typeof(IPatientConst).GetProperties())
            {
                if (!p.GetValue(this).Equals(p.GetValue(backupCopy)))
                {
                    NotifyPropertyChanged(p.Name);
                }
            }
            backupCopy = null;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void NotifyPropertyChanged(string propName)
        {
            this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propName));
        }

        public override string ToString()
        {
            return Name;
        }
    }

    public class PatientValidationRule : ValidationRule
    {
        private DutyData dutyData;

        public PatientValidationRule(DutyData dutyData)
        {
            this.dutyData = dutyData;
        }

        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            var patient = (value as BindingGroup).Items[0] as Patient;
            bool isValid = true;
            var errorContent = "";

            var match = Regex.Match(patient.Name, @"^(\w|_)(\w|\d)*$", RegexOptions.IgnoreCase);
            if (!match.Success)
            {
                errorContent += "Nazwisko nie może być puste, nie może zaczynać się od cyfry i może składać się tylko z liter, cyfr i znaku _";
                isValid = false;
            }

            foreach (var p in dutyData.Patients)
            {
                if (p != patient && p.Name == patient.Name)
                {
                    if (errorContent.Length > 0) errorContent += '\n';
                    errorContent += "Nazwisko pacjenta musi być unikalne";
                    isValid = false;
                    break;
                }
            }

            if (patient.AvailableSumFrom > patient.AvailableSumTo)
            {
                if (errorContent.Length > 0) errorContent += '\n';
                errorContent += "Minimalna ilość dyżurów nie może być większa od maksymalnej";
                isValid = false;
            }

            return new ValidationResult(isValid, errorContent);
        }
    }

    public class HistoryValidationRule : ValidationRule
    {
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            if (value == null)
            {
                return new ValidationResult(false, "Brak lub nieprawidołowe nazwisko pacjenta");
            }
            return ValidationResult.ValidResult;
        }
    }

    public class Duty : INotifyPropertyChanged
    {
        private Pair duzaSwietlica;
        private Pair malaSwietlica;
        private Pair wystawianieKolacji;
        private Pair jadalnia;
        private Pair suszarnia;
        private Pair zmywanieSniadanie;
        private Pair zmywanieObiad;


        [DisplayedValue("Duża\nświetlica", 1)]
        public Pair DuzaSwietlica
        {
            get => duzaSwietlica;
            set
            {
                if (duzaSwietlica != value)
                {
                    duzaSwietlica = value;
                    NotifyPropertyChanged(nameof(DuzaSwietlica));
                }
            }
        }

        [DisplayedValue("Mała\nświetlica", 2)]
        public Pair MalaSwietlica
        {
            get => malaSwietlica;
            set
            {
                if (malaSwietlica != value)
                {
                    malaSwietlica = value;
                    NotifyPropertyChanged(nameof(MalaSwietlica));
                }
            }
        }

        [DisplayedValue("Wystawianie\nkolacji", 3)]
        public Pair WystawianieKolacji
        {
            get => wystawianieKolacji;
            set
            {
                if (wystawianieKolacji != value)
                {
                    wystawianieKolacji = value;
                    NotifyPropertyChanged(nameof(WystawianieKolacji));
                }
            }
        }

        [DisplayedValue("Jadalnia", 4)]
        public Pair Jadalnia
        {
            get => jadalnia;
            set
            {
                if (jadalnia != value)
                {
                    jadalnia = value;
                    NotifyPropertyChanged(nameof(Jadalnia));
                }
            }
        }

        [DisplayedValue("Suszarnia", 5)]
        public Pair Suszarnia
        {
            get => suszarnia;
            set
            {
                if (suszarnia != value)
                {
                    suszarnia = value;
                    NotifyPropertyChanged(nameof(Suszarnia));
                }
            }
        }

        [DisplayedValue("Zmywanie po\nśniadaniu", 6)]
        public Pair ZmywanieSniadanie
        {
            get => zmywanieSniadanie;
            set
            {
                if (zmywanieSniadanie != value)
                {
                    zmywanieSniadanie = value;
                    NotifyPropertyChanged(nameof(ZmywanieSniadanie));
                }
            }
        }

        [DisplayedValue("Zmywanie po\nobiedzie", 7)]
        public Pair ZmywanieObiad
        {
            get => zmywanieObiad;
            set
            {
                if (zmywanieObiad != value)
                {
                    zmywanieObiad = value;
                    NotifyPropertyChanged(nameof(ZmywanieObiad));
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void NotifyPropertyChanged(string propName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propName));
        }

        public override string ToString()
        {
            var result = "{";
            foreach (var p in DisplayedValue.GetOrderedProperties(typeof(Duty)))
            {
                result += p.Name + "=" + p.GetValue(this) + ",";
            }
            result += "}";
            return result;
        }
    }

    public class Settings : INotifyPropertyChanged
    {
        private string miniZincPath;
        private string miniZincArgs;
        private uint miniZincTimeout;
        private bool miniZincShowLogs;
        private bool isSettingChanged;

        public void LoadSettings()
        {
            MiniZincPath = Properties.Settings.Default.MiniZincPath;
            MiniZincArgs = Properties.Settings.Default.MiniZincArgs;
            MiniZincTimeout = Properties.Settings.Default.MiniZincTimeout;
            MiniZincShowLogs = Properties.Settings.Default.MiniZincShowLogs;
            IsSettingChanged = false;
        }

        public void SaveSettings()
        {
            Properties.Settings.Default.MiniZincPath = MiniZincPath;
            Properties.Settings.Default.MiniZincArgs = MiniZincArgs;
            Properties.Settings.Default.MiniZincTimeout = MiniZincTimeout;
            Properties.Settings.Default.MiniZincShowLogs = MiniZincShowLogs;
            Properties.Settings.Default.Save();
            IsSettingChanged = false;
        }

        public bool IsSettingChanged
        {
            get => isSettingChanged;
            set
            {
                if (isSettingChanged != value)
                {
                    isSettingChanged = value;
                    NotifyPropertyChanged(nameof(IsSettingChanged));
                }
            }
        }

        public string MiniZincPath
        {
            get => miniZincPath;
            set
            {
                if (miniZincPath != value)
                {
                    miniZincPath = value;
                    NotifyPropertyChanged(nameof(MiniZincPath));
                    IsSettingChanged = true;
                }
            }
        }

        public string MiniZincArgs
        {
            get => miniZincArgs;
            set
            {
                if (miniZincArgs != value)
                {
                    miniZincArgs = value;
                    NotifyPropertyChanged(nameof(MiniZincArgs));
                    IsSettingChanged = true;
                }
            }
        }

        public uint MiniZincTimeout
        {
            get => miniZincTimeout;
            set
            {
                if (miniZincTimeout != value)
                {
                    miniZincTimeout = value;
                    NotifyPropertyChanged(nameof(MiniZincTimeout));
                    IsSettingChanged = true;
                }
            }
        }

        public bool MiniZincShowLogs
        {
            get => miniZincShowLogs;
            set
            {
                if (miniZincShowLogs != value)
                {
                    miniZincShowLogs = value;
                    NotifyPropertyChanged(nameof(MiniZincShowLogs));
                    IsSettingChanged = true;
                }
            }
        }

        public void RefreshMinizincPath()
        {
            if (String.IsNullOrEmpty(MiniZincPath))
            {
                if (File.Exists(GlobalVars.MZN_COMPILER_FILE))
                {
                    MiniZincPath = GlobalVars.MZN_COMPILER_FILE;
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void NotifyPropertyChanged(string propName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propName));
        }
    }

    public class DutyData : INotifyPropertyChanged
    {
        public class InvalidDataException : Exception
        {
            public InvalidDataException(string message) : base("uszkodzony plik danych: " + message)
            {
            }
        }

        public ObservableCollection<Patient> Patients { get; } = new ObservableCollection<Patient>();
        public ObservableCollection<Duty> History { get; } = new ObservableCollection<Duty>();
        public Settings Settings { get; } = new Settings();

        public Patient DischargedPatient { get; private set; }

        public event PropertyChangedEventHandler PropertyChanged;
        public delegate void DutyDataChangedHandler();

        private DateTime currentDate = DateTime.Now;
        private bool isDataChanged = false;

        public DutyData()
        {
            Patients.CollectionChanged += Patients_CollectionChanged;
            History.CollectionChanged += History_CollectionChanged;
        }

        public bool IsDataChanged
        {
            get => isDataChanged;
            set
            {
                if (isDataChanged != value)
                {
                    isDataChanged = value;
                    NotifyPropertyChanged(nameof(IsDataChanged));
                }
            }
        }

        public DateTime CurrentDate
        {
            get => currentDate;
            set
            {
                if (currentDate != value)
                {
                    currentDate = value;
                    NotifyPropertyChanged(nameof(CurrentDate));
                    IsDataChanged = true;
                }
            }
        }

        public void LoadData(out Exception backupException, bool loadBackup)
        {
            backupException = null;
            try
            {
                string path = loadBackup ? GlobalVars.MZN_DATA_BACKUP_FILE : GlobalVars.MZN_DATA_FILE;
                Patients.Clear();
                History.Clear();
                DischargedPatient = null;

                var input = File.ReadAllText(path);
                input = Regex.Replace(input, @"%[^\r\n]*", "");

                var matched = Regex.Match(input, @"CURRENT_DATE\s*=\s*\""([^""]+)\""");
                if (!matched.Success)
                {
                    throw new InvalidDataException("brak lub niepoprawna sekcja CURRENT_DATE");
                }
                try
                {
                    CurrentDate = DateTime.ParseExact(matched.Groups[1].Captures[0].Value, GlobalVars.DATE_FORMAT, CultureInfo.InvariantCulture);
                }
                catch (FormatException ex)
                {
                    throw new InvalidDataException("niepoprawna sekcja CURRENT_DATE: " + ex.Message);
                }

                matched = Regex.Match(input, @"PATIENT\s*=\s*\{(?:\s*(\w+)\s*,?)+\}");
                if (!matched.Success)
                {
                    throw new InvalidDataException("brak lub niepoprawna sekcja PATIENT");
                }
                foreach (var c in matched.Groups[1].Captures)
                {
                    Patients.Add(new Patient(c.ToString()));
                }

                matched = Regex.Match(input, @"available\s*=\s*\[(?:\s*\|(?:\s*([01])\s*,?)+)+\|\]");
                if (!matched.Success || (matched.Groups[1].Captures.Count % 4) != 0 ||
                    ((matched.Groups[1].Captures.Count / 4) != Patients.Count))
                {
                    throw new InvalidDataException("brak lub niepoprawna sekcja available");
                }
                var cc = matched.Groups[1].Captures;
                for (int i = 0; i < Patients.Count; i++)
                {
                    Patients[i].AvailableWeek1 = uint.Parse(cc[4 * i + 0].Value) != 0;
                    Patients[i].AvailableWeek2 = uint.Parse(cc[4 * i + 1].Value) != 0;
                    Patients[i].AvailableWeek3 = uint.Parse(cc[4 * i + 2].Value) != 0;
                    Patients[i].AvailableWeek4 = uint.Parse(cc[4 * i + 3].Value) != 0;
                }

                matched = Regex.Match(input, @"available_sum\s*=\s*\[(?:\s*\|(?:\s*([01234])\s*,?)+)+\|\]");
                if (!matched.Success || (matched.Groups[1].Captures.Count % 2) != 0 ||
                    ((matched.Groups[1].Captures.Count / 2) != Patients.Count))
                {
                    throw new InvalidDataException("brak lub niepoprawna sekcja available_sum");
                }
                cc = matched.Groups[1].Captures;
                for (int i = 0; i < Patients.Count; i++)
                {
                    Patients[i].AvailableSumFrom = uint.Parse(cc[2 * i + 0].Value);
                    Patients[i].AvailableSumTo = uint.Parse(cc[2 * i + 1].Value);
                }

                var props = DisplayedValue.GetOrderedProperties(typeof(Duty)).ToArray();

                matched = Regex.Match(input, @"history\s*=[^\[]*\[(?:\s*(\w+)\s*,?)*\s*\]");
                if (!matched.Success || (matched.Groups[1].Captures.Count % (2 * props.Length)) != 0)
                {
                    throw new InvalidDataException("brak lub niepoprawna sekcja history");
                }

                cc = matched.Groups[1].Captures;
                try
                {
                    for (int i = 0; i < cc.Count / (props.Length * 2); i++)
                    {
                        var duty = new Duty();
                        for (int j = 0; j < props.Length; j++)
                        {

                            props[j].SetValue(duty, new Pair(Patients.First(p => p.Name == cc[2 * props.Length * i + 2 * j + 0].Value),
                                                             Patients.First(p => p.Name == cc[2 * props.Length * i + 2 * j + 1].Value)));
                        }
                        History.Add(duty);
                    }
                }
                catch (InvalidOperationException)
                {
                    throw new InvalidDataException("historia zawiera nieistniejącego pacjenta.");
                }
                if (!loadBackup)
                {
                    SaveBackup(out backupException, input);
                }
                IsDataChanged = false;
            }
            finally
            {
                DischargedPatient = Patients.FirstOrDefault(p => p.Name == GlobalVars.DISCHARGED_STR);
                if (DischargedPatient == null)
                {
                    DischargedPatient = new Patient(GlobalVars.DISCHARGED_STR);
                    Patients.Add(DischargedPatient);
                }
                TrimHistory();
            }
        }

        public string ToMznString(bool shufflePatients)
        {
            var output = "";
            Collection<Patient> patientsView;
            if (shufflePatients)
            {
                patientsView = new Collection<Patient>();
                Random rnd = new Random();
                foreach (var p in Patients.OrderBy(x => rnd.Next()))
                {
                    patientsView.Add(p);
                }
            }
            else
            {
                patientsView = Patients;
            }

            output += "PATIENT = {\n";
            for (int i = 0; i < patientsView.Count; i++)
            {
                output += "\t" + patientsView[i].Valid().Name;
                if ((i + 1) != patientsView.Count)
                {
                    output += ',';
                }
                output += '\n';
            }
            output += "};\n\n";

            output += "available = [|\n\t ";
            for (int i = 0; i < patientsView.Count; i++)
            {
                output +=
                    (patientsView[i].Valid().AvailableWeek1 ? 1 : 0) + "," +
                    (patientsView[i].Valid().AvailableWeek2 ? 1 : 0) + "," +
                    (patientsView[i].Valid().AvailableWeek3 ? 1 : 0) + "," +
                    (patientsView[i].Valid().AvailableWeek4 ? 1 : 0);
                if (i + 1 != patientsView.Count)
                {
                    output += ',';
                }
                output += "\t% " + patientsView[i].Valid().Name + "\n\t|";
            }
            output += "];\n\n";

            output += "available_sum = [|\n\t ";
            for (int i = 0; i < patientsView.Count; i++)
            {
                output += patientsView[i].Valid().AvailableSumFrom + "," + patientsView[i].Valid().AvailableSumTo;
                if (i + 1 != patientsView.Count)
                {
                    output += ',';
                }
                output += "\t% " + patientsView[i].Valid().Name + "\n\t|";
            }
            output += "];\n\n";

            output += "WEEKS_NUMBER = " + History.Count + ";\n\n";

            output += "CURRENT_DATE = \"" + CurrentDate.ToString(GlobalVars.DATE_FORMAT) + "\";\n\n";

            output += "history = array3d(HISTORY_TIME, DUTY, PAIR, [\n";
            for (int i = 0; i < History.Count; i++)
            {
                output += '\t';
                var props = DisplayedValue.GetOrderedProperties(typeof(Duty)).ToArray();
                for (int j = 0; j < props.Length; j++)
                {
                    var p = props[j].GetValue(History[i]) as Pair;
                    output += p.A.Valid().Name + ',' + p.B.Valid().Name;
                    if (i + 1 != History.Count || j + 1 != props.Length)
                    {
                        output += ',';
                    }
                }
                output += '\n';
                if (((i + 1) % 4) == 0 && (i + 1) != History.Count)
                {
                    output += '\n';
                }
            }
            output += "]);\n";
            return output;
        }

        public void SaveData(out Exception backupException, bool saveBackup)
        {
            backupException = null;
            TrimHistory();
            var output = ToMznString(false);

            if (saveBackup)
            {
                SaveBackup(out backupException, output);
            }

            Directory.CreateDirectory(Path.GetDirectoryName(GlobalVars.MZN_DATA_FILE));
            File.WriteAllText(GlobalVars.MZN_DATA_FILE, output);
            IsDataChanged = false;
        }

        private void SaveBackup(out Exception backupException, string output)
        {
            backupException = null;
            try
            {
                Directory.CreateDirectory(Path.GetDirectoryName(GlobalVars.MZN_DATA_BACKUP_FILE));
                File.WriteAllText(GlobalVars.MZN_DATA_BACKUP_FILE, output);
            }
            catch (SystemException ex)
            {
                backupException = ex;
            }
        }

        private void TrimHistory()
        {
            while (History.Count >= 4)
            {
                for (int i = 0; i < 4; i++)
                {
                    var duty = History[History.Count - 1 - i];
                    foreach (var p1 in DisplayedValue.GetOrderedProperties(typeof(Duty)))
                    {
                        var pair = p1.GetValue(duty) as Pair;
                        foreach (var p2 in DisplayedValue.GetOrderedProperties(typeof(Pair)))
                        {
                            var patient = p2.GetValue(pair) as Patient;
                            if (patient != DischargedPatient)
                            {
                                goto NotAllDischarged;
                            }
                        }
                    }
                }

                for (int i = 0; i < 4; i++)
                {
                    History.RemoveAt(History.Count - 1);
                }
            }
            NotAllDischarged:
            ;
        }

        private void History_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.OldItems != null)
            {
                foreach (Duty item in e.OldItems)
                {
                    foreach (var p1 in DisplayedValue.GetOrderedProperties(typeof(Duty)))
                    {
                        var pair = p1.GetValue(item) as Pair;
                        pair.PropertyChanged -= DataChanged;
                    }
                }
            }
            if (e.NewItems != null)
            {
                foreach (INotifyPropertyChanged item in e.NewItems)
                {
                    foreach (var p1 in DisplayedValue.GetOrderedProperties(typeof(Duty)))
                    {
                        var pair = p1.GetValue(item) as Pair;
                        pair.PropertyChanged += DataChanged;
                    }
                }
            }
            IsDataChanged = true;
        }

        private void Patients_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.OldItems != null)
            {
                foreach (INotifyPropertyChanged item in e.OldItems)
                {
                    item.PropertyChanged -= DataChanged;
                }
            }
            if (e.NewItems != null)
            {
                foreach (INotifyPropertyChanged item in e.NewItems)
                {
                    item.PropertyChanged += DataChanged;
                }
            }

            if (e.Action == NotifyCollectionChangedAction.Remove)
            {
                foreach (var duty in History)
                {
                    foreach (var p1 in DisplayedValue.GetOrderedProperties(typeof(Duty)))
                    {
                        var pair = p1.GetValue(duty) as Pair;
                        foreach (var p2 in DisplayedValue.GetOrderedProperties(typeof(Pair)))
                        {
                            var patient2 = p2.GetValue(pair) as Patient;
                            foreach (Patient patient1 in e.OldItems)
                            {
                                if (patient1.Name == patient2.Name)
                                {
                                    p2.SetValue(pair, DischargedPatient);
                                }
                            }
                        }
                    }
                }
                TrimHistory();
            }
            IsDataChanged = true;
        }

        private void DataChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName != nameof(Patient.IsSelected))
            {
                IsDataChanged = true;
            }
        }

        public void NotifyPropertyChanged(string propName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propName));
        }
    }
}
