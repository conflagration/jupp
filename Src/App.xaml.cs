﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;

namespace JUPP
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private const int MINIMUM_SPLASH_TIME = 1000;
        private const int SPLASH_FADE_TIME = 2000;

        protected override void OnStartup(StartupEventArgs e)
        {
            SplashScreen splash = new SplashScreen("resources/graphics/splash.png");
            
            splash.Show(false, true);

            Stopwatch timer = new Stopwatch();

            timer.Start();
            base.OnStartup(e);
            MainWindow main = new MainWindow();
            timer.Stop();

            int remainingTimeToShowSplash = MINIMUM_SPLASH_TIME - (int)timer.ElapsedMilliseconds;
            if (remainingTimeToShowSplash > 0)
            {
                Thread.Sleep(remainingTimeToShowSplash);
            }

            splash.Close(TimeSpan.FromMilliseconds(SPLASH_FADE_TIME));
            main.Show();
        }
    }
}
