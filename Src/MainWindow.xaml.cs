﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Threading;

namespace JUPP
{
    [ValueConversion(typeof(string), typeof(string))]
    public class RatioConverter : MarkupExtension, IValueConverter
    {
        private static RatioConverter _instance;

        public RatioConverter() { }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            double size = System.Convert.ToDouble(value) * System.Convert.ToDouble(parameter, CultureInfo.InvariantCulture);
            return size.ToString("G0", CultureInfo.InvariantCulture);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return _instance ?? (_instance = new RatioConverter());
        }
    }
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public static RoutedCommand InsertMonthBeforeCmd = new RoutedCommand();
        public static RoutedCommand InsertMonthAfterCmd = new RoutedCommand();
        public static RoutedCommand DeleteMonthCmd = new RoutedCommand();
        public static RoutedCommand AddPatientCmd = new RoutedCommand();
        public static RoutedCommand DeletePatientCmd = new RoutedCommand();
        public static RoutedCommand ClearOutputCmd = new RoutedCommand();
        public static RoutedCommand CancelGenerationCmd = new RoutedCommand();
        public static RoutedCommand SaveAllCmd = new RoutedCommand();
        public static RoutedCommand ExportToImageCmd = new RoutedCommand();
        public static RoutedCommand AboutCmd = new RoutedCommand();
        public static RoutedCommand HelpCmd = new RoutedCommand();

        private Process MznProcess;
        private string MznOutput;
        private string MznDataPath;
        private string MznProgPath;
        private DispatcherTimer MznTimer;

        private Style rowStyle1 = new Style(typeof(DataGridRow));
        private Style rowStyle2 = new Style(typeof(DataGridRow));
        private Style rowStyleNew = new Style(typeof(DataGridRow));
        private SolidColorBrush selectedCellBrush;
        private bool newSechdule;


        private static bool IsValidObject(DependencyObject parent)
        {
            if (Validation.GetHasError(parent))
            {
                return false;
            }

            for (int i = 0; i != VisualTreeHelper.GetChildrenCount(parent); ++i)
            {
                var child = VisualTreeHelper.GetChild(parent, i);
                if (!IsValidObject(child))
                {
                    return false;
                }
            }
            return true;
        }

        private static T FindParent<T>(DependencyObject child) where T : DependencyObject
        {
            DependencyObject parentObject = VisualTreeHelper.GetParent(child);

            if (parentObject == null)
            {
                return null;
            }

            //check if the parent matches the type we're looking for
            T parent = parentObject as T;
            if (parent != null)
            {
                return parent;
            }
            else
            {
                return FindParent<T>(parentObject);
            }
        }

        private static string GetExecutingDirectory()
        {
            var location = new Uri(Assembly.GetEntryAssembly().GetName().CodeBase);
            return Path.GetDirectoryName(location.LocalPath);
        }

        private bool HasFocus(Control aControl, bool aCheckChildren)
        {
            var oFocused = System.Windows.Input.FocusManager.GetFocusedElement(this) as DependencyObject;
            if (!aCheckChildren)
            {
                return oFocused == aControl;
            }
            while (oFocused != null)
            {
                if (oFocused == aControl)
                {
                    return true;
                }
                oFocused = VisualTreeHelper.GetParent(oFocused);
            }
            return false;
        }

        public MainWindow()
        {
            InitializeComponent();


            SetupStyles();

            var dutyData = new DutyData();
            DataContext = dutyData;
            dutyData.PropertyChanged += DutyData_PropertyChanged;
            dutyData.Settings.PropertyChanged += Settings_PropertyChanged;
            dutyData.Settings.LoadSettings();

            ClearOutput();
            CheckMiniZinc();

            Exception backupException = null;
            Exception dummy;
            try
            {
                dutyData.LoadData(out backupException, false);
            }
            catch (Exception ex) when (ex is SystemException || ex is DutyData.InvalidDataException)
            {
                var msg = "Nie można odczytać pliku danych: ";
                OutputError(msg + ex.Message);

                if (File.Exists(GlobalVars.MZN_DATA_BACKUP_FILE))
                {
                    var result = MessageBox.Show(msg + ex.Message + "\nZaleca się załadowanie pliku danych z kopii zapasowej. Czy załadować plik danych z kopii zapasowej?",
                        Title, MessageBoxButton.YesNo, MessageBoxImage.Error);
                    if (result == MessageBoxResult.Yes)
                    {
                        try
                        {
                            dutyData.LoadData(out dummy, true);
                            try
                            {
                                dutyData.SaveData(out dummy, false);
                            }
                            catch (Exception ex2) when (ex2 is SystemException || ex2 is DutyData.InvalidDataException)
                            {
                                OutputError("Nie można zapisać pliku danych: " + ex2.Message);
                            }
                        }
                        catch (Exception ex2) when (ex2 is SystemException || ex2 is DutyData.InvalidDataException)
                        {
                            OutputError(msg + ex2.Message);
                        }
                    }
                }
            }

            if (backupException != null)
            {
                OutputWarning("Nie można zapisać kopii zapasowej danych: " + backupException.Message);
            }

            SetupPatientsCtrl();
            SetupHistoryCtrl();
            PatientsCtrl.Focus();
        }

        private void Datagrid_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            var dataGrid = sender as DataGrid;
            if (Keyboard.IsKeyDown(Key.LeftCtrl) || Keyboard.IsKeyDown(Key.RightCtrl))
            {
                var row = FindParent<DataGridRow>(e.OriginalSource as DependencyObject);
                if (row != null)
                {
                    dataGrid.Focus();
                    if (row.IsSelected)
                    {
                        dataGrid.SelectedItems.Remove(row.Item);
                    }
                    else
                    {
                        dataGrid.SelectedItems.Add(row.Item);
                    }
                    e.Handled = true;
                }
            }
        }

        private void PatientSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var dutyData = DataContext as DutyData;
            if (e.AddedItems != null)
            {
                foreach (var item in e.AddedItems)
                {
                    var patient = item as Patient;
                    if (patient != null)
                    {
                        patient.IsSelected = true;
                    }
                }
            }

            if (e.RemovedItems != null)
            {
                foreach (var item in e.RemovedItems)
                {
                    var patient = item as Patient;
                    if (patient != null)
                    {
                        patient.IsSelected = false;
                    }
                }
            }
        }

        private DataGridColumn CreateHistoryColumn(PropertyInfo p1, PropertyInfo p2, DutyData dutyData)
        {
            var dataGridTemplate = new DataGridTemplateColumn();
            var name = p1.Name + "." + p2.Name;

            Thickness cellThickness;
            Thickness headerThickness;
            if (HistoryCtrl.Columns.Count == 0)
            {
                cellThickness = new Thickness(1.0, 0.0, 0.0, 0.0);
                headerThickness = new Thickness(1.0, 0.5, 0.0, 1.0);
            }
            else if ((HistoryCtrl.Columns.Count % 2) == 0)
            {
                cellThickness = new Thickness(0.0, 0.0, 0.0, 0.0);
                headerThickness = new Thickness(0.0, 0.5, 0.0, 1.0);
            }
            else
            {
                cellThickness = new Thickness(0.0, 0.0, 0.25, 0.0);
                headerThickness = new Thickness(0.0, 0.5, 1.0, 1.0);
            }

            dataGridTemplate.HeaderStyle = new Style(typeof(DataGridColumnHeader));
            dataGridTemplate.HeaderStyle.Setters.Add(new Setter(DataGridColumnHeader.BorderThicknessProperty, headerThickness));
            dataGridTemplate.HeaderStyle.Setters.Add(new Setter(DataGridColumnHeader.BorderBrushProperty, HistoryCtrl.HorizontalGridLinesBrush));
            dataGridTemplate.HeaderStyle.Setters.Add(new Setter(DataGridColumnHeader.HorizontalContentAlignmentProperty, HorizontalAlignment.Center));
            dataGridTemplate.HeaderStyle.Setters.Add(new Setter(DataGridColumnHeader.VerticalAlignmentProperty, VerticalAlignment.Center));


            dataGridTemplate.CellStyle = new Style(typeof(DataGridCell));
            dataGridTemplate.CellStyle.Setters.Add(new Setter(DataGridCell.BorderThicknessProperty, cellThickness));
            dataGridTemplate.CellStyle.Setters.Add(new Setter(DataGridCell.BorderBrushProperty, HistoryCtrl.HorizontalGridLinesBrush));
            var dataTrigger = new DataTrigger();
            dataTrigger.Binding = new Binding(name + "." + nameof(Patient.IsSelected));
            dataTrigger.Value = true;
            dataTrigger.Setters.Add(new Setter(DataGridCell.BackgroundProperty, selectedCellBrush));
            dataGridTemplate.CellStyle.Triggers.Add(dataTrigger);

            var comboBoxFactory = new FrameworkElementFactory(typeof(ComboBox));

            var dataView = new ListCollectionView(dutyData.Patients);
            dataView.SortDescriptions.Add(new SortDescription(nameof(Patient.Name), ListSortDirection.Ascending));
            if (dataView.CanChangeLiveSorting)
            {
                dataView.IsLiveSorting = true;
            }
            comboBoxFactory.SetValue(ComboBox.ItemsSourceProperty, dataView);
            comboBoxFactory.SetValue(ComboBox.IsEditableProperty, true);
            comboBoxFactory.SetValue(ComboBox.DisplayMemberPathProperty, nameof(Patient.Name));
            comboBoxFactory.SetValue(ComboBox.SelectedValuePathProperty, nameof(Patient.Name));
            
            var selectedItemBinding = new Binding(name);
            selectedItemBinding.Mode = BindingMode.TwoWay;
            selectedItemBinding.UpdateSourceTrigger = UpdateSourceTrigger.LostFocus;
            var validationRule = new HistoryValidationRule();
            selectedItemBinding.ValidationRules.Add(validationRule);

            comboBoxFactory.SetBinding(ComboBox.SelectedItemProperty, selectedItemBinding);
            var selectedValueBinding = new Binding(name + "." + nameof(Patient.Name));
            selectedValueBinding.Mode = BindingMode.OneWay;
            comboBoxFactory.SetBinding(ComboBox.TextProperty, selectedValueBinding);
            comboBoxFactory.AddHandler(ComboBox.LoadedEvent, new RoutedEventHandler(HistoryCtrl_ComboBoxLoaded));
            comboBoxFactory.AddHandler(ComboBox.UnloadedEvent, new RoutedEventHandler(HistoryCtrl_ComboBoxUnloaded));
            comboBoxFactory.AddHandler(ComboBox.SelectionChangedEvent, new SelectionChangedEventHandler(HistoryCtrl_ComboBoxSelectionChanged));
            var headerText = p1.GetCustomAttribute<DisplayedValue>().Value;
            headerText += headerText.Contains('\n') ? ' ' : '\n';
            headerText += p2.GetCustomAttribute<DisplayedValue>().Value;
            dataGridTemplate.Header = headerText;

            var textBlockFactory = new FrameworkElementFactory(typeof(TextBlock));
            textBlockFactory.SetBinding(TextBlock.TextProperty, new Binding(name + "." + nameof(Patient.Name)) { Mode = BindingMode.OneWay });

            dataGridTemplate.CellEditingTemplate = new DataTemplate() { VisualTree = comboBoxFactory };
            dataGridTemplate.CellTemplate = new DataTemplate() { VisualTree = textBlockFactory };
            dataGridTemplate.ClipboardContentBinding = selectedValueBinding;

            return dataGridTemplate;
        }

        private void SetupHistoryCtrl()
        {
            var dutyData = DataContext as DutyData;
            foreach (var p1 in DisplayedValue.GetOrderedProperties(typeof(Duty)))
            {
                foreach (var p2 in DisplayedValue.GetOrderedProperties(typeof(Pair)))
                {
                    HistoryCtrl.Columns.Add(CreateHistoryColumn(p1, p2, dutyData));
                }
            }
        }

        private void SetupPatientsCtrl()
        {
            var dutyData = DataContext as DutyData;
            var validationRule = new PatientValidationRule(DataContext as DutyData);
            validationRule.ValidationStep = ValidationStep.UpdatedValue;
            PatientsCtrl.RowValidationRules.Add(validationRule);

            var column = PatientsCtrl.Columns[0];
            var dataView = new ListCollectionView(dutyData.Patients);
            PatientsCtrl.ItemsSource = dataView;
            dataView.SortDescriptions.Clear();
            dataView.SortDescriptions.Add(new SortDescription(column.SortMemberPath, column.SortDirection ?? ListSortDirection.Ascending));
            dataView.Filter = p => p != dutyData.DischargedPatient;
            dataView.Refresh();

            PatientsCtrl.SelectedIndex = -1;
        }

        private void CommitChanges()
        {
            OutputCtrl.Focus();
            HistoryCtrl.CommitEdit();
            PatientsCtrl.CommitEdit();
        }

        private bool IsDataValid()
        {
            return IsValidObject(PatientsCtrl) && IsValidObject(HistoryCtrl);
        }

        private bool PromptIfValidationErrors()
        {
            CommitChanges();
            if (!IsDataValid())
            {
                MessageBox.Show("Operacja nie może być przeprowadzona gdyż tabela zawiera błędne dane. Popraw dane aby kontynuować operację.",
                    Title, MessageBoxButton.OK, MessageBoxImage.Error);
                return false;
            }
            else
            {
                return true;
            }
        }

        private void OutputLog(string msg)
        {
            Paragraph p = new Paragraph(new Run(msg));
            OutputCtrl.Document.Blocks.Add(p);
        }

        private void OutputError(string msg)
        {
            Paragraph p = new Paragraph(new Run(msg));
            p.Foreground = Brushes.Red;
            OutputCtrl.Document.Blocks.Add(p);
        }

        private void OutputWarning(string msg)
        {
            Paragraph p = new Paragraph(new Run(msg));
            p.Foreground = Brushes.Goldenrod;
            OutputCtrl.Document.Blocks.Add(p);
        }

        private void OutputInfo(string msg)
        {
            Paragraph p = new Paragraph(new Run(msg));
            p.Foreground = Brushes.Green;
            OutputCtrl.Document.Blocks.Add(p);
        }

        private void ClearOutput()
        {
            OutputCtrl.Document.Blocks.Clear();
            OutputInfo("Witaj w programie do tworzenia grafików dyżurów dla 7F!");
        }

        private void InsertHistoryRow(int rowIdx)
        {
            HistoryCtrl.CommitEdit();
            var dutyData = DataContext as DutyData;

            int modelIdx;
            if (rowIdx > 0 && rowIdx < HistoryCtrl.Items.Count)
            {
                modelIdx = dutyData.History.IndexOf(HistoryCtrl.Items[rowIdx] as Duty);
            }
            else if (rowIdx >= HistoryCtrl.Items.Count)
            {
                modelIdx = HistoryCtrl.Items.Count;
            }
            else
            {
                modelIdx = 0;
            }

            if (modelIdx == 0)
            {
                dutyData.CurrentDate = dutyData.CurrentDate.AddDays(7 * 4);
            }

            for (int i = 0; i < 4; i++)
            {
                var duty = new Duty();
                foreach (var p in DisplayedValue.GetOrderedProperties(typeof(Duty)))
                {
                    p.SetValue(duty, new Pair(dutyData.DischargedPatient, dutyData.DischargedPatient));
                }
                dutyData.History.Insert(modelIdx + i, duty);
            }
            if (rowIdx == 0)
            {
                newSechdule = false;
                if (HistoryCtrl.Items.Count > 0)
                {
                    SwapStyles();
                }
            }
            HistoryCtrl.Focus();
            HistoryCtrl.SelectedIndex = rowIdx;
            HistoryCtrl.ScrollIntoView(HistoryCtrl.SelectedItem);
        }

        private void DeleteHistoryRow(Duty duty)
        {
            var dutyData = DataContext as DutyData;
            var modelIdx = dutyData.History.IndexOf(duty);
            var rowIdx = HistoryCtrl.Items.IndexOf(duty);

            if (modelIdx == 0)
            {
                dutyData.CurrentDate = dutyData.CurrentDate.AddDays(-(7 * 4));
            }

            for (int i = 0; i < 4; i++)
            {
                dutyData.History.RemoveAt(modelIdx);
            }

            if (rowIdx == 0)
            {
                newSechdule = false;
                SwapStyles();
            }

            HistoryCtrl.Focus();
            if (rowIdx < HistoryCtrl.Items.Count)
            {
                HistoryCtrl.SelectedIndex = rowIdx;
            }
            else
            {
                HistoryCtrl.SelectedIndex = HistoryCtrl.Items.Count - 1;
            }

            if (HistoryCtrl.SelectedItem != null)
            {
                HistoryCtrl.ScrollIntoView(HistoryCtrl.SelectedItem);
            }
        }

        private bool SaveData()
        {
            if (PromptIfValidationErrors())
            {
                var dutyData = DataContext as DutyData;
                try
                {
                    Exception backupException;
                    dutyData.SaveData(out backupException, true);
                    if (backupException != null)
                    {
                        OutputWarning("Nie można zapisać kopii zapasowej danych: " + backupException.Message);
                    }
                    newSechdule = false;
                    Dispatcher.BeginInvoke(new Action(() => HistoryCtrl.Items.Refresh()));
                    return true;
                }
                catch (Exception ex) when (ex is SystemException || ex is DutyData.InvalidDataException)
                {
                    OutputError("Nie można zapisać pliku danych: " + ex.Message);
                }
            }
            return false;
        }

        private bool SaveSettings()
        {
            var dutyData = DataContext as DutyData;
            dutyData.Settings.SaveSettings();
            return true;
        }

        private bool SaveAll()
        {
            return SaveData() && SaveSettings();
        }

        private void InsertMonthBeforeCmd_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = TabCtrl.SelectedIndex == 0;
        }

        private void InsertMonthBeforeCmd_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            InsertHistoryRow((HistoryCtrl.SelectedIndex / 4) * 4);
            HistoryCtrl.Items.Refresh();
        }

        private void InsertMonthAfterCmd_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = TabCtrl.SelectedIndex == 0;
        }

        private void InsertMonthAfterCmd_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var dutyData = DataContext as DutyData;
            if (HistoryCtrl.SelectedIndex == -1)
            {
                InsertHistoryRow(dutyData.History.Count);
            }
            else
            {
                InsertHistoryRow((HistoryCtrl.SelectedIndex / 4) * 4 + 4);
            }
            HistoryCtrl.Items.Refresh();
        }

        private void DeleteMonthCmd_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            var dutyData = DataContext as DutyData;
            e.CanExecute = TabCtrl.SelectedIndex == 0 && dutyData.History.Count > 0;
        }

        private void DeleteMonthCmd_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var duties = new HashSet<Duty>();
            if (HistoryCtrl.SelectedItem != null)
            {
                foreach (var item in HistoryCtrl.SelectedItems)
                {
                    var idx = (HistoryCtrl.Items.IndexOf(item) / 4) * 4;
                    var duty = HistoryCtrl.Items[idx] as Duty;
                    duties.Add(duty);
                }

                foreach (var duty in duties)
                {
                    DeleteHistoryRow(duty);
                }
            }
            else
            {
                DeleteHistoryRow(HistoryCtrl.Items[0] as Duty);
            }
            HistoryCtrl.Items.Refresh();
        }

        private void AddPatientCmd_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = TabCtrl.SelectedIndex == 0;
        }

        private void AddPatientCmd_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            PatientsCtrl.CommitEdit();

            var dutyData = DataContext as DutyData;
            var patient = new Patient();
            int maxNum = 0;
            foreach (var p in dutyData.Patients.Where(p => p.Name.StartsWith(patient.Name)))
            {
                try
                {
                    var currNum = int.Parse(p.Name.Substring(patient.Name.Length));
                    if (currNum > maxNum)
                    {
                        maxNum = currNum;
                    }
                }
                catch (FormatException) { }
            }

            patient.Name += maxNum + 1;
            dutyData.Patients.Add(patient);

            PatientsCtrl.Focus();
            PatientsCtrl.SelectedItem = patient;
            PatientsCtrl.ScrollIntoView(patient);
            PatientsCtrl.CurrentCell = new DataGridCellInfo(PatientsCtrl.SelectedItem, PatientsCtrl.Columns[0]);
            PatientsCtrl.BeginEdit();
        }

        private void DeletePatientCmd_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            var dutyData = DataContext as DutyData;
            e.CanExecute = TabCtrl.SelectedIndex == 0 && PatientsCtrl.SelectedItem != null && dutyData.Patients.Count > 0 &&
                PatientsCtrl.SelectedItem != dutyData.DischargedPatient;
        }

        private void DeletePatientCmd_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var dutyData = DataContext as DutyData;
            var rowIdx = int.MaxValue;
            var snap = new List<Patient>();
            foreach (var o in PatientsCtrl.SelectedItems)
            {
                var p = o as Patient;
                if (p != null)
                {
                    snap.Add(p);
                }
            }

            foreach (Patient p in snap)
            {
                var idx = HistoryCtrl.Items.IndexOf(p);
                rowIdx = Math.Min(rowIdx, idx);
                dutyData.Patients.Remove(p);
            }

            PatientsCtrl.Focus();
            if (rowIdx < PatientsCtrl.Items.Count)
            {
                PatientsCtrl.SelectedIndex = rowIdx;
            }
            else
            {
                PatientsCtrl.SelectedIndex = PatientsCtrl.Items.Count - 1;
            }

            if (PatientsCtrl.SelectedItem != null)
            {
                PatientsCtrl.ScrollIntoView(PatientsCtrl.SelectedItem);
            }
        }

        private void ClearOutput_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = TabCtrl.SelectedIndex == 0;
        }

        private void ClearOutput_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            ClearOutput();
        }

        private void SaveCmd_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void SaveCmd_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            if (TabCtrl.SelectedIndex == 0)
            {
                SaveData();
            }
            else
            {
                SaveSettings();
            }
        }

        private void SaveAllCmd_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void SaveAllCmd_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            SaveAll();
        }

        private void CloseCmd_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void CloseCmd_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            Close();
        }

        private void CopyCmd_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void CopyCmd_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            ApplicationCommands.Copy.Execute(null, sender as IInputElement);
        }

        private void ExportToImageCmd_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void ExportToImageCmd_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            Control control = HistoryCtrl;

            Microsoft.Win32.SaveFileDialog dlg = new Microsoft.Win32.SaveFileDialog();
            dlg.FileName = "dyzury";
            dlg.DefaultExt = ".png";
            dlg.Filter = "Plik graficzny PNG|*.png|Wszystkie|*";

            RenderTargetBitmap rtb = new RenderTargetBitmap((int)control.ActualWidth, (int)control.ActualHeight, 96, 96, PixelFormats.Pbgra32);
            rtb.Render(control);

            PngBitmapEncoder png = new PngBitmapEncoder();
            png.Frames.Add(BitmapFrame.Create(rtb));

            Nullable<bool> result = dlg.ShowDialog();
            if (result == true)
            {
                FileStream fs = null;
                try
                {
                    fs = File.Create(dlg.FileName);
                    png.Save(fs);
                }
                catch (SystemException ex)
                {
                    MessageBox.Show("Eksport do pliku nie powiódł się: " + ex.Message);
                }
                finally
                {
                    fs?.Dispose();
                }

                try
                {
                    Process.Start(dlg.FileName).Dispose();
                }
                catch(SystemException) { }
            }
        }

        private void AboutCmd_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void AboutCmd_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var aboutBox = new AboutBox();
            aboutBox.Owner = this;
            aboutBox.ShowDialog();
        }

        private void HelpCmd_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void HelpCmd_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var startInfo = new ProcessStartInfo(Path.Combine(GetExecutingDirectory(), "JUPP.chm"));
            startInfo.WindowStyle = ProcessWindowStyle.Maximized;
            startInfo.UseShellExecute = true;
            try
            {
                Process.Start(startInfo).Dispose();
            }
            catch (SystemException ex)
            {
                OutputError("Nie można wyświetlić pomocy: " + ex.Message);
            }
        }

        private void GenerateScheduleCmd_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = TabCtrl.SelectedIndex == 0 && MznProcess == null;
        }

        private void GenerateScheduleCmd_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            if (!PromptIfValidationErrors())
            {
                return;
            }
            
            bool cleanup = true;
            try
            {
                Dispatcher.BeginInvoke(new Action(() => HistoryCtrl.Items.Refresh()));
                ProgressCtrl.Visibility = Visibility.Visible;
                ProgressCtrl.IsIndeterminate = true;
                Mouse.OverrideCursor = Cursors.AppStarting;
                newSechdule = false;
                ClearOutput();

                var dutyData = DataContext as DutyData;

                var output = dutyData.ToMznString(true);
                var tmpFolder = System.IO.Path.GetTempPath();
                MznDataPath = Path.Combine(tmpFolder, Path.GetRandomFileName()) + ".dzn";
                MznProgPath += Path.Combine(tmpFolder, Path.GetRandomFileName()) + ".mzn";

                try
                {
                    File.WriteAllText(MznDataPath, output);
                    File.WriteAllBytes(MznProgPath, Properties.Resources.Duties);
                }
                catch (SystemException ex)
                {
                    OutputError("Stworzenie grafiku nie powiodło się: " + ex.Message);
                    return;
                }

                MznOutput = "";
                MznProcess = new Process();
                MznProcess.StartInfo.FileName = dutyData.Settings.MiniZincPath;
                MznProcess.StartInfo.Arguments = dutyData.Settings.MiniZincArgs + " --time-limit " + (1000 * dutyData.Settings.MiniZincTimeout) +
                    " \"" + MznDataPath + "\" \"" + MznProgPath + "\"";
                MznProcess.StartInfo.UseShellExecute = false;
                MznProcess.StartInfo.CreateNoWindow = true;
                MznProcess.StartInfo.RedirectStandardOutput = true;
                MznProcess.StartInfo.RedirectStandardError = true;
                MznProcess.EnableRaisingEvents = true;
                MznProcess.OutputDataReceived += MznProcess_OutputDataReceived;
                MznProcess.ErrorDataReceived += MznProcess_ErrorDataReceived;
                MznProcess.Exited += MznProcess_Exited;

                try
                {
                    MznProcess.Start();
                    cleanup = false;
                }
                catch (SystemException ex)
                {
                    OutputError("Stworzenie grafiku nie powiodło się: " + ex.Message);
                    return;
                }

                MznTimer = new DispatcherTimer();
                MznTimer.Tick += MznTimer_Tick;
                MznTimer.Interval = new TimeSpan(0, 0, (int)dutyData.Settings.MiniZincTimeout + 10);
                MznTimer.Start();

                OutputInfo("Rozpoczęto tworzenie grafiku...");
                MznProcess.BeginOutputReadLine();
                MznProcess.BeginErrorReadLine();
            }
            finally
            {
                if (cleanup)
                {
                    if (MznProcess != null)
                    {
                        MznProcess.Dispose();
                    }

                    if (MznProgPath != null)
                    {
                        try
                        {
                            File.Delete(MznProgPath);
                        }
                        catch (SystemException ex)
                        {
                            OutputWarning("Nie można usunąć tymczasowego pliku: " + ex.Message);
                        }
                    }


                    if (MznDataPath != null)
                    {
                        try
                        {
                            File.Delete(MznDataPath);
                        }
                        catch (SystemException ex)
                        {
                            OutputWarning("Nie można usunąć tymczasowego pliku: " + ex.Message);
                        }
                    }

                    ProgressCtrl.IsIndeterminate = false;
                    ProgressCtrl.Value = 100;
                    ProgressCtrl.Visibility = Visibility.Collapsed;
                    Mouse.OverrideCursor = null;
                    
                    MznTimer = null;
                    MznOutput = null;
                    MznProgPath = null;
                    MznDataPath = null;
                    MznProcess = null;
                }
            }
        }

        private void CancelGenerationCmd_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = MznProcess != null;
        }

        private void CancelGenerationCmd_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            MznTimer.Stop();
            try
            {
                MznProcess.Kill();
            }
            catch(SystemException) { }
        }

        private void MznTimer_Tick(object sender, EventArgs e)
        {
            MznTimer.Stop();
            if (MznProcess != null)
            {
                try
                {
                    MznProcess.Kill();
                }
                catch(SystemException) { }
                var dutyData = DataContext as DutyData;
                OutputWarning("Tworzenie grafiku przekroczyło limit czasowy " + dutyData.Settings.MiniZincTimeout + "s.");
            }
        }

        private void MznProcess_ErrorDataReceived(object sender, DataReceivedEventArgs e)
        {
            if (!String.IsNullOrEmpty(e.Data))
            {
                Dispatcher.Invoke(() =>
                {
                    OutputError(e.Data);
                    OutputCtrl.ScrollToEnd();
                });
            }
        }

        private void MznProcess_OutputDataReceived(object sender, DataReceivedEventArgs e)
        {
            if (!String.IsNullOrEmpty(e.Data))
            {
                Dispatcher.Invoke(() =>
                {
                    MznOutput += e.Data + "\n";
                    var dutyData = DataContext as DutyData;
                    if (dutyData.Settings.MiniZincShowLogs)
                    {
                        OutputLog(e.Data);
                        OutputCtrl.ScrollToEnd();
                    }
                });
            }
        }

        private void MznProcess_Exited(object sender, EventArgs e)
        {
            MznTimer.Stop();
            MznProcess.WaitForExit();
            int exitCode = MznProcess.ExitCode;
            MznProcess.Dispose();

            SystemException ex1 = null;
            SystemException ex2 = null;

            try
            {
                File.Delete(MznProgPath);
            }
            catch (SystemException ex)
            {
                ex1 = ex;
            }

            try
            {
                File.Delete(MznDataPath);
            }
            catch(SystemException ex)
            {
                ex2 = ex;
            }

            Dispatcher.Invoke(() =>
            {
                if (exitCode != 0)
                {
                    OutputWarning("Kod wyjścia MiniZinc: " + exitCode + ".");
                }
                TransferResults();

                if (ex1 != null)
                {
                    OutputWarning("Nie można usunąć tymczasowego pliku: " + ex1.Message);
                }

                if (ex2 != null)
                {
                    OutputWarning("Nie można usunąć tymczasowego pliku: " + ex2.Message);
                }

                MznTimer = null;
                MznOutput = null;
                MznProgPath = null;
                MznDataPath = null;

                Mouse.OverrideCursor = null;
                OutputCtrl.ScrollToEnd();
                ProgressCtrl.IsIndeterminate = false;
                ProgressCtrl.Value = 100;

                var timer = new DispatcherTimer() { Interval = new TimeSpan(0, 0, 0, 0, 1000) };
                timer.Tick += (_sender, _e) =>
                {
                    (_sender as DispatcherTimer).Stop();
                    ProgressCtrl.Visibility = Visibility.Collapsed;
                    MznProcess = null;
                };
                timer.Start();
            });
        }

        private void SetupStyles()
        {
            rowStyle1.Setters.Add(new Setter(DataGridRow.BackgroundProperty,FindResource("RowColor1")));
            rowStyle2.Setters.Add(new Setter(DataGridRow.BackgroundProperty, FindResource("RowColor2")));
            rowStyleNew.Setters.Add(new Setter(DataGridRow.BackgroundProperty, FindResource("RowColorNew")));
            selectedCellBrush = (SolidColorBrush)FindResource("CellColorSelected");
        }

        private void SwapStyles()
        {
            var tmp = rowStyle1;
            rowStyle1 = rowStyle2;
            rowStyle2 = tmp;
        }

        private void TransferResults()
        {
            var props = DisplayedValue.GetOrderedProperties(typeof(Duty)).ToArray();
            
            var matches = Regex.Matches(MznOutput, @"duty_patient\s*=[^\[]*\[(?:\s*(\w+)\s*,?)*\s*\]", RegexOptions.Multiline);
            if (matches.Count <= 0)
            {
                OutputError("Stworzenie grafiku nie powiodło się.");
                return;
            }

            var match = matches[matches.Count - 1];
            if (!match.Success || (match.Groups[1].Captures.Count % (2 * props.Length)) != 0)
            {
                OutputError("Stworzenie grafiku nie powiodło się.");
                return;
            }

            var dutyData = DataContext as DutyData;
            var cc = match.Groups[1].Captures;
            var duties = new List<Duty>();
            for (int i = 0; i < cc.Count / (props.Length * 2); i++)
            {
                var duty = new Duty();
                for (int j = 0; j < props.Length; j++)
                {
                    try
                    {
                        props[j].SetValue(duty, new Pair(dutyData.Patients.First(p => p.Name == cc[2 * props.Length * i + 2 * j + 0].Value),
                                                         dutyData.Patients.First(p => p.Name == cc[2 * props.Length * i + 2 * j + 1].Value)));
                    }
                    catch (InvalidOperationException)
                    {
                        OutputError("Stworzenie grafiku nie powiodło się: niepoprawny format danych.");
                        return;
                    }
                }
                duties.Add(duty);
            }

            for (int i = 0; i < duties.Count; i++)
            {
                dutyData.History.Insert(i, duties[i]);
            }

            newSechdule = true;
            SwapStyles();
            dutyData.CurrentDate = dutyData.CurrentDate.AddDays(7 * 4);
            Dispatcher.BeginInvoke(new Action(() => HistoryCtrl.Items.Refresh()));
            HistoryCtrl.Focus();
            HistoryCtrl.SelectedIndex = 0;
            HistoryCtrl.ScrollIntoView(HistoryCtrl.SelectedItem);

            OutputInfo("Grafik dla okresu od " + dutyData.CurrentDate.ToString(GlobalVars.DATE_FORMAT) + " do " +
                dutyData.CurrentDate.AddDays(7 * 4 - 1).ToString(GlobalVars.DATE_FORMAT) + " został stworzony!");
            matches = Regex.Matches(MznOutput, @"obj1\s*=\s*(\d+)", RegexOptions.Multiline);
            if (matches.Count > 0)
            {
                match = matches[matches.Count - 1];
                cc = match.Groups[1].Captures;
                OutputLog("Maksymalna ilość dyżurów dla jednej osoby: " + cc[0]);
            }

            matches = Regex.Matches(MznOutput, @"obj3\s*=\s*(\d+)", RegexOptions.Multiline);
            if (matches.Count > 0)
            {
                match = matches[matches.Count - 1];
                cc = match.Groups[1].Captures;
                OutputLog("Liczba osób, które powtórzyły dyżury z poprzedniego miesiaca: " + cc[0]);
            }

            matches = Regex.Matches(MznOutput, @"obj4\s*=\s*(\d+)", RegexOptions.Multiline);
            if (matches.Count > 0)
            {
                match = matches[matches.Count - 1];
                cc = match.Groups[1].Captures;
                OutputLog("Liczba dyżurów, w których obydwie osoby nigdy nie wykonywały tego dyżuru lub obydwie go kiedyś wykonały: " + cc[0]);
            }

            matches = Regex.Matches(MznOutput, @"obj5\s*=\s*(\d+)", RegexOptions.Multiline);
            if (matches.Count > 0)
            {
                match = matches[matches.Count - 1];
                cc = match.Groups[1].Captures;
                OutputLog("Sumaryczna liczba dyżurów, które zostały powtórzone przez tę samą osobę: " + cc[0]);
            }

            OutputLog("Czy rozwiązanie jest optymalne: " + (MznOutput.Contains("==========") ? "TAK" : "NIE"));
        }

        private void CheckMiniZinc(uint iter = 0)
        {
            var dutyData = DataContext as DutyData;
            dutyData.Settings.RefreshMinizincPath();
            var miniZincPath = dutyData.Settings.MiniZincPath;
            Task.Run(() =>
            {
                Process process = new Process();
                process.StartInfo.FileName = miniZincPath;
                process.StartInfo.UseShellExecute = false;
                process.StartInfo.CreateNoWindow = true;
                bool processStarted = false;
                try
                {
                    process.Start();
                    processStarted = true;
                }
                catch (SystemException ex1)
                {
                    Dispatcher.Invoke(() => {
                        if (iter == 0)
                        {
                            var res = MessageBox.Show("Nie wykryto środowiska MiniZinc, bez którego nie można tworzyć nowych dyżurów. "  +
                                "Zaleca się instalację MiniZinc przy pierwszym uruchomieniu programu. Czy zainstalować MiniZinc?",
                                Title, MessageBoxButton.YesNo, MessageBoxImage.Question);
                            if (res == MessageBoxResult.Yes)
                            {
                                Mouse.OverrideCursor = Cursors.Wait;
                                Task.Run(() => {
                                    Process proc = null;
                                    try
                                    {
                                        try
                                        {
                                            proc = Process.Start(@"mzn\MiniZincIDE.exe");
                                        }
                                        catch (SystemException ex2)
                                        {
                                            Dispatcher.BeginInvoke(new Action(() => MessageBox.Show("Nie można zainstalować środowiska MiniZinc: " + ex2.Message, Title, MessageBoxButton.OK, MessageBoxImage.Error)));
                                        }
                                    }
                                    finally
                                    {
                                        if (proc != null)
                                        {
                                            try
                                            {
                                                proc.WaitForExit();
                                            }
                                            catch (SystemException) { }
                                            proc.Dispose();
                                            Dispatcher.BeginInvoke(new Action(() => {
                                                CheckMiniZinc(iter + 1);
                                                Mouse.OverrideCursor = null;
                                            }));
                                        }
                                    }
                                });
                            }
                            else
                            {
                                OutputWarning("Nie wykryto środowiska MiniZinc, bez którego nie można tworzyć nowych dyżurów: " + ex1.Message + ". " +
                                              "Sprawdź w ustawieniach czy ścieżka do MiniZinc jest poprawna.");
                            }
                        }
                        else
                        {
                            OutputWarning("Nie udało się zainstalować środowiska MiniZinc: " + ex1.Message);
                        }
                    });
                }
                finally
                {
                    try
                    {
                        process.Kill();
                    }
                    catch (SystemException) { }
                    process.Dispose();
                }

                if (iter > 0 && processStarted)
                {
                    Dispatcher.Invoke(() => OutputInfo("MiniZinc został poprawnie zainstalowany!"));
                }
            });
        }

        private void HistoryCtrl_ComboBoxLoaded(object sender, RoutedEventArgs e)
        {
            var comboBox = sender as ComboBox;
            var textBox = comboBox.Template.FindName("PART_EditableTextBox", comboBox) as TextBox;
            textBox.Focus();
            textBox.SelectAll();
        }

        private void HistoryCtrl_ComboBoxUnloaded(object sender, RoutedEventArgs e)
        {
            var comboBox = sender as ComboBox;
            var patient = comboBox.SelectedItem as Patient;
            patient.IsSelected = false;
            PatientsCtrl.SelectedItems.Remove(patient);
        }

        private void HistoryCtrl_ComboBoxSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            PatientSelectionChanged(sender, e);
            if (e.AddedItems != null)
            {
                foreach (Patient p in e.AddedItems)
                {
                    PatientsCtrl.SelectedItems.Add(p);
                    PatientsCtrl.ScrollIntoView(p);
                }
            }

            if (e.RemovedItems != null)
            {
                foreach (Patient p in e.RemovedItems)
                {
                    PatientsCtrl.SelectedItems.Remove(p);
                }
            }
        }

        private void HistoryCtrl_LoadingRow(object sender, DataGridRowEventArgs e)
        {
            var dutyData = DataContext as DutyData;

            var idx = e.Row.GetIndex();
            var dt = dutyData.CurrentDate.AddDays((-(idx/4)*4 + (idx % 4)) * 7);
            e.Row.Header = dt.ToString(GlobalVars.DATE_FORMAT);


            if (newSechdule && idx < 4) e.Row.Style = rowStyleNew;
            else if ((idx % 8) < 4) e.Row.Style = rowStyle1;
            else e.Row.Style = rowStyle2;
        }

        private void HistoryCtrl_LostFocus(object sender, RoutedEventArgs e)
        {
            HistoryCtrl.SelectedIndex = -1;
        }

        private void HistoryCtrl_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            Datagrid_PreviewMouseLeftButtonDown(sender, e);
        }

        private void PatientsCtrl_LoadingRow(object sender, DataGridRowEventArgs e)
        {
            e.Row.Header = (e.Row.GetIndex() + 1).ToString();
        }

        private void PatientsCtrl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            PatientSelectionChanged(sender, e);
        }

        private void PatientsCtrl_LostFocus(object sender, RoutedEventArgs e)
        {
            PatientsCtrl.SelectedIndex = -1;
        }

        private void PatientsCtrl_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            Datagrid_PreviewMouseLeftButtonDown(sender, e);
        }

        private void Calendar_SelectedDatesChanged(object sender, SelectionChangedEventArgs e)
        {
            CommitChanges();
            HistoryCtrl.Items.Refresh();
            MenuEditionCtrl.IsSubmenuOpen = false;
            HistoryCtrl.Focus();
        }

        private void DutyData_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(DutyData.IsDataChanged))
            {
                var dutyData = sender as DutyData;
                var tabItem = (TabCtrl.Items[0] as TabItem);
                if (dutyData.IsDataChanged)
                {
                    tabItem.Header += "*";
                }
                else
                {
                    var header = tabItem.Header as string;
                    tabItem.Header = header.Remove(header.Length - 1);
                }
            }
        }

        private void Settings_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(Settings.IsSettingChanged))
            {
                var settings = sender as Settings;
                var tabItem = (TabCtrl.Items[1] as TabItem);
                if (settings.IsSettingChanged)
                {
                    tabItem.Header += "*";
                }
                else
                {
                    var header = tabItem.Header as string;
                    tabItem.Header = header.Remove(header.Length - 1);
                }
            }
        }

        private void MainWindowCtrl_Closing(object sender, CancelEventArgs e)
        {
            CommitChanges();
            var dutyData = DataContext as DutyData;

            if (!IsDataValid())
            {
                var result = MessageBox.Show("Dane nie będą mogły być zapisane gdyż tabela zawiera błedne wpisy. Czy na pewno wyjść z programu?",
                    Title, MessageBoxButton.YesNo, MessageBoxImage.Error);
                if (result == MessageBoxResult.No)
                {
                    e.Cancel = true;
                }
                return;
            }

            if (dutyData.IsDataChanged || dutyData.Settings.IsSettingChanged)
            {
                var result = MessageBox.Show("Czy chcesz zapisać zmiany?", Title,
                    MessageBoxButton.YesNoCancel, MessageBoxImage.Question);
                if (result == MessageBoxResult.Yes)
                {
                    if (!SaveAll())
                    {
                        result = MessageBox.Show("Zapis nie powiódł się. Czy na pewno chcesz zamknąć aplikację?",
                            Title, MessageBoxButton.YesNo, MessageBoxImage.Error);
                        e.Cancel = result == MessageBoxResult.No;
                    }
                }
                else if (result == MessageBoxResult.Cancel)
                {
                    e.Cancel = true;
                }
            }

        }

        private void MainWindowCtrl_Closed(object sender, EventArgs e)
        {
            if (MznProcess != null)
            {
                MznTimer.Stop();
                try
                {
                    MznProcess.Kill();
                }
                catch(SystemException) { }
            }
        }
    }
}
